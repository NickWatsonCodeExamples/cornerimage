//
//  ViewController.swift
//  CornerLabelViewDemo
//
//  Created by Nick Watson on 01/08/2018.
//  Copyright © 2018 Nick Watson. All rights reserved.
//

import UIKit
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageList.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let viewWidth = view.frame.width
        let viewHeight = view.frame.height
        let cellSize = CGSize(width: viewWidth/2, height: viewHeight/3)
        return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CustomCell
        let cellCorner = cell.cornerImage!
        cellCorner.imageString = "Swift.png"
        
        switch indexPath.row {
        case 2:
            cellCorner.isHidden = true
        case 5:
            cellCorner.isHidden = true
        case 8:
            cellCorner.isHidden = true
            
        default:
            break
        }
        let imageStr = imageList[indexPath.row].description
        cell.displayImage(imageString: imageStr)
        return cell
    }
}
class ViewController: UIViewController {
    
    private var imageList = [
        "0.png", "1.png",
        "2.png", "3.png",
        "4.png", "5.png",
        "6.png", "7.png",
        "8.png", "9.png"]
    
    
    @IBOutlet weak var collectionList:UICollectionView? {
        didSet {
            collectionList?.backgroundColor = UIColor.white
            //register nibs for collectionview cell
            collectionList?.register(UINib(nibName: "CustomCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}



