//
//  CustomCell.swift
//  CornerLabelViewDemo
//
//  Created by Nick Watson on 01/08/2018.
//  Copyright © 2018 Nick Watson. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {
  
    var cornerImage:CornerImageView!
  
    @IBOutlet weak var mainImage:UIImageView?
  
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerImage = CornerImageView(frame: bounds)
        addSubview(cornerImage)
    }
   
    func displayImage(imageString:String) {
        let image: UIImage = UIImage(named: imageString)!
        mainImage?.image = image.scaleImage(toSize: CGSize(width: 150, height: 150))
    }
}

extension UIImage {
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(self.cgImage!, in: newRect)
            let newImage = UIImage(cgImage: context.makeImage()!)
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
}
