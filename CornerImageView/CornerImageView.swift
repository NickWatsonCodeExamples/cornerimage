//
//  CornerImageView.swift
//  CornerImageView
//
//  Created by Nick Watson on 01/08/2018.
//  Copyright © 2018 Nick Watson. All rights reserved.
//

import UIKit

@IBDesignable public class CornerImageView: UIView {
    
    private var length = CGFloat()
    private var newRect = CGRect()
    
    // MARK: - Initialization
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.isOpaque = false
    }
    
    // MARK: - Attributes
    
    /// The postion of the view for the Iterface Builder.
    @available(*, unavailable, message: "This property is reserved for Iterface Builder. Use position instead")
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    /// The text displayed by the label.
    @IBInspectable public var imageString:String = "" {
        didSet {
            setNeedsDisplay()
        }
    }
    
    /// The background color of the view.
    @IBInspectable private var viewColor:UIColor = UIColor.clear {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // MARK: - Drawing
    
    /// Create a new rectangle according to the position.
    private func updateNewRect() {
        let rectWidth = bounds.width
        let rectHeight = bounds.height
        
        length = 0.5 * min(rectWidth, rectHeight)
        
        newRect = CGRect(x: rectWidth - length, y: 0, width: length, height: length)
        
    }
    
    override public func draw(_ rect: CGRect) {
        updateNewRect()
        
        let cornerPath = UIBezierPath()
        var pointValues = [CGFloat]()
        let rectOriginX = newRect.origin.x
        let rectOriginY = newRect.origin.y
        let rectWidth = newRect.width + rectOriginX
        let rectHeight = newRect.width + rectOriginY
        
        // Default is TopRight
        pointValues = [rectOriginX,rectOriginY, rectWidth, rectOriginY, rectWidth, rectHeight, rectOriginX, rectOriginY]
            
        
        cornerPath.move(to: CGPoint(x: pointValues[0], y: pointValues[1]))
        cornerPath.addLine(to: CGPoint(x: pointValues[2], y: pointValues[3]))
        cornerPath.addLine(to: CGPoint(x: pointValues[4], y: pointValues[5]))
        cornerPath.addLine(to: CGPoint(x: pointValues[6], y: pointValues[7]))
        cornerPath.close()
        
        UIColor.clear.setStroke()
        viewColor.setFill()
        cornerPath.fill()
        cornerPath.stroke()
        
        addImageToView(cornerImage: imageString)
    }
    
    // MARK: - Image
    
    /// Add the Image to the view.
    private func addImageToView(cornerImage: String) {
        let (x, y) = getImgPostion(newRect.width)
        let imageName = cornerImage
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: x, y: y, width: (newRect.width / 2) , height: (newRect.height / 2))
        self.addSubview(imageView)
    }
    
    /// Get the position of the label inside the view.
    private func getImgPostion(_ length: CGFloat) -> ( CGFloat, CGFloat) {
        var x = CGFloat()
        var y = CGFloat()
        
        let rectOriginX = newRect.origin.x
        let rectOriginY = newRect.origin.y
        let rectWidth = newRect.width

        x = (2/3*rectWidth+rectOriginX) - 20 //-textWidth/2
        y = (1/3*rectWidth+rectOriginY) - 20 //-textHeight/2
        
        return(x, y)
    }
}

/// Clear the child views.
private extension UIView {
    func clearChildViews() {
        subviews.forEach({ $0.removeFromSuperview() })
    }
}
